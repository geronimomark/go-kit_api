package main

import (
	"encoding/json"
	"errors"
	"time"

	"os"
	"strconv"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/asaskevich/govalidator"
)

// JobQueueService provides operations on job queue.
type JobQueueService interface {
	SaveJob(map[string]interface{}) (string, error)
	GetJob(string) ([]Job, error)
	UpdateJob(map[string]interface{}) (string, error)
}

type jobQueueService struct{}

type Job struct {
	ID                  bson.ObjectId          `bson:"_id,omitempty"`
	Job_Title           string                 `json:"job_title" valid:"required"`
	Job_Description     string                 `json:"job_description" valid:"required"`
	Seller_ID           int                    `json:"seller_id" valid:"required"`
	Folder_ID           int                    `json:"folder_id" valid:"required"`
	User_ID             int                    `json:"user_id" valid:"required"`
	Send_Email          int                    `json:"send_email" valid:"required"`
	Date_Created        time.Time              `json:"date_created"`
	Date_Completed      time.Time              `json:"date_completed"`
	Done                bool                   `json:"done"`
	Succeed             bool                   `json:"succeed"`
	Error_Short_Message string                 `json:"error_short_message"`
	Error_Long_Message  string                 `json:"error_long_message"`
	Thread_ID           string                 `json:"thread_id"`
	Status              string                 `json:"status"`
	Job_Type            string                 `json:"job_type" valid:"required"`
	Dynamic_Details     map[string]interface{} `json:"dynamic_details" valid:"-"`
	Job_Details         map[string]interface{} `json:"job_details" valid:"-"`
}

type UpdateJob struct {
	ID     bson.ObjectId `json:"id" bson:"_id,omitempty" valid:"required"`
	Status string        `json:"status" valid:"required"`
}

func (jobQueueService) SaveJob(s map[string]interface{}) (string, error) {
	if len(s) == 0 {
		return "", ErrEmpty
	}
	// do stuff here

	session, err := mgo.Dial("mongo:" + os.Getenv("DB_PORT"))
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB(os.Getenv("DB_NAME")).C(os.Getenv("DB_TABLE"))

	job := Job{}

	data, err := json.Marshal(s)
	if err != nil {
		panic(err)
	}
	json.Unmarshal(data, &job)

	result, err := govalidator.ValidateStruct(job)
	if result != true {
		b := strconv.FormatBool(result)
		return b, err
	}

	dynamicMap := make(map[string]interface{})
	detailsMap := make(map[string]interface{})

	for k, v := range job.Dynamic_Details {
		dynamicMap[k] = v
	}

	for k, v := range job.Job_Details {
		detailsMap[k] = v
	}

	if len(dynamicMap) == 0 || len(detailsMap) == 0 {
		return "Empty details", ErrEmpty
	}

	job.Date_Created = time.Now()
	job.Date_Completed = time.Now()
	job.Done = false
	job.Succeed = false
	job.Error_Short_Message = ""
	job.Error_Long_Message = ""
	job.Status = "queued"
	job.Dynamic_Details = dynamicMap
	job.Job_Details = detailsMap

	err = c.Insert(job)

	var response = "success"

	if err != nil {
		// panic(err)
		response = "failed"
	}

	return response, err
}

func (jobQueueService) GetJob(p string) ([]Job, error) {
	session, err := mgo.Dial("mongo:" + os.Getenv("DB_PORT"))
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB(os.Getenv("DB_NAME")).C(os.Getenv("DB_TABLE"))
	// Query All
	var results []Job
	err = c.Find(bson.M{"job_type": p, "status": "queued"}).All(&results)

	if err != nil {
		panic(err)
	}

	// data, err := json.Marshal(results)
	// if err != nil {
	// 	panic(err)
	// }

	return results, nil
}

func (jobQueueService) UpdateJob(s map[string]interface{}) (string, error) {
	if len(s) == 0 {
		return "", ErrEmpty
	}
	// do stuff here

	session, err := mgo.Dial("mongo:" + os.Getenv("DB_PORT"))
	if err != nil {
		panic(err)
	}
	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	c := session.DB(os.Getenv("DB_NAME")).C(os.Getenv("DB_TABLE"))

	updateJob := UpdateJob{}
	data, err := json.Marshal(s)
	if err != nil {
		return "error", err
	}
	json.Unmarshal(data, &updateJob)

	where := bson.M{"_id": updateJob.ID}
	change := bson.M{"$set": bson.M{"status": updateJob.Status, "date_completed": time.Now()}}
	err = c.Update(where, change)

	var response = "success"

	if err != nil {
		// panic(err)
		response = "failed"
	}

	return response, err
}

// ErrEmpty is returned when json string is empty
var ErrEmpty = errors.New("Empty JSON Parameter")

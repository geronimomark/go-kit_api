# Job Queue Service

Job Queue Service is a microservice Golang application.

### Installation

Job Queue Service requires Go v1.6, Glide, Docker and MongoDB v3.2 to run

Install the dependencies and start the server.

```sh
$ mkdir /data/docker/mongo
$ sudo chcon -Rt svirt_sandbox_file_t /data/docker/mongo
$ cd /your/desired/golang-app/directory/
$ git clone https://gitlab.usautoparts.com/omp/job-queue-service.git
$ cd job-queue-service
$ glide update
$ docker-compose up
```

Services
 - SaveJob
 - GetJob
 - UpdateJob

### SaveJob
###### URL - http://localhost:8080/JobQueue/SaveJob
###### Method - POST
###### Content-type - application/json
###### Parameters:
 - data: json object

Sample request data
```javascript
{
    "data" : {
		"job_title" : "Title 3",
		"job_description" : "Descriptionxxx",
		"seller_id" : 1,
		"folder_id" : 2,
		"user_id" : 3,
		"send_email" : 4,
		"job_type" : "listing",
		"dynamic_details" : {
			"test1" : "data1",
			"test2" : "data2"
		},
		"job_details" : {
			"test3" : "data3"
		}
	}
}
```
Sample response
```javascript
{
  "result": "success"
}
```

### GetJob
###### URL - http://localhost:8080/JobQueue/GetJob
###### Method - GET
###### Parameters:
 - type: string <listing|revise|relist|add|edit>

Sample request
```html
http://localhost:8080/JobQueue/GetJob?type=listing
```
Sample response
```javascript
{
    "result" : [
        {
            "job_title" : "Title 3",
            "job_description" : "Descriptionxxx",
            "seller_id" : 1,
            "folder_id" : 2,
            "user_id" : 3,
            "send_email" : 4,
            "job_type" : "listing",
            "dynamic_details" : {
                "test1" : "data1",
                "test2" : "data2"
            },
            "job_details" : {
                "test3" : "data3"
            }
        },
        {
            "job_title" : "Title 5",
            "job_description" : "Descriptionyyy",
            "seller_id" : 1,
            "folder_id" : 2,
            "user_id" : 3,
            "send_email" : 4,
            "job_type" : "listing",
            "dynamic_details" : {
                "test1" : "data1",
                "test2" : "data2",
                "test3" : "data3"
            },
            "job_details" : {
                "test3" : 5
            }
	    }
    ]
}
```

### UpdateJob
###### URL - http://localhost:8080/JobQueue/UpdateJob
###### Method - PUT
###### Content-type - application/json
###### Parameters:
 - data: json object

Sample request data
```javascript
{
    "data" : {
		"id":"57e0b1837c1d1310b85a22f4",
		"status" : "done"
	}
}
```
Sample response
```javascript
{
  "result": "success"
}
```
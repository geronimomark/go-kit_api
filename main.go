package main

import (
	"net/http"
	"os"
	"time"

	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"golang.org/x/net/context"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	httptransport "github.com/go-kit/kit/transport/http"
)

func main() {

	ctx := context.Background()
	logger := log.NewLogfmtLogger(os.Stderr)

	fieldKeys := []string{"method", "error"}
	requestCount := kitprometheus.NewCounter(stdprometheus.CounterOpts{
		Namespace: "my_group",
		Subsystem: "joq_queue_service",
		Name:      "request_count",
		Help:      "Number of requests received.",
	}, fieldKeys)
	requestLatency := metrics.NewTimeHistogram(time.Microsecond, kitprometheus.NewSummary(stdprometheus.SummaryOpts{
		Namespace: "my_group",
		Subsystem: "joq_queue_service",
		Name:      "request_latency_microseconds",
		Help:      "Total duration of requests in microseconds.",
	}, fieldKeys))
	countResult := kitprometheus.NewSummary(stdprometheus.SummaryOpts{
		Namespace: "my_group",
		Subsystem: "joq_queue_service",
		Name:      "count_result",
		Help:      "The result of each count method.",
	}, []string{}) // no fields here

	var svc JobQueueService
	svc = jobQueueService{}
	svc = loggingMiddleware{logger, svc}
	svc = instrumentingMiddleware{requestCount, requestLatency, countResult, svc}

	saveJobHandler := httptransport.NewServer(
		ctx,
		saveJobEndpoint(svc),
		decodeSaveJobRequest,
		encodeResponse,
	)

	getJobHandler := httptransport.NewServer(
		ctx,
		getJobEndpoint(svc),
		decodeGetJobRequest,
		encodeResponse,
	)

	updateJobHandler := httptransport.NewServer(
		ctx,
		updateJobEndpoint(svc),
		decodeUpdateJobRequest,
		encodeResponse,
	)

	http.Handle(os.Getenv("SAVE_SERVICE"), saveJobHandler)
	http.Handle(os.Getenv("GET_SERVICE"), getJobHandler)
	http.Handle(os.Getenv("UPDATE_SERVICE"), updateJobHandler)
	http.Handle(os.Getenv("METRICS_SERVICE"), stdprometheus.Handler())
	logger.Log("msg", "HTTP", "addr", ":"+os.Getenv("MY_PORT"))
	logger.Log("err", http.ListenAndServe(":"+os.Getenv("MY_PORT"), nil))
}

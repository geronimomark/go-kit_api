package main

import (
	"encoding/json"
	"errors"
	"net/http"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/endpoint"
)

func saveJobEndpoint(svc JobQueueService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(saveJobRequest)
		v, err := svc.SaveJob(req.S)
		if err != nil {
			return saveJobResponse{v, err.Error()}, nil
		}
		return saveJobResponse{v, ""}, nil
	}
}

func getJobEndpoint(svc JobQueueService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getJobRequest)
		v, err := svc.GetJob(req.S)
		if err != nil {
			return getJobResponse{v, err.Error()}, nil
		}
		return getJobResponse{v, ""}, nil
	}
}

func updateJobEndpoint(svc JobQueueService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateJobRequest)
		v, err := svc.UpdateJob(req.S)
		if err != nil {
			return updateJobResponse{v, err.Error()}, nil
		}
		return updateJobResponse{v, ""}, nil
	}
}

func decodeSaveJobRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request saveJobRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeGetJobRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request getJobRequest
	jtype := r.URL.Query().Get("type")
	if jtype == "" {
		return nil, errors.New("Empty Parameter")
	}
	request.S = jtype
	return request, nil
}

func decodeUpdateJobRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request updateJobRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(response)
}

type saveJobRequest struct {
	S map[string]interface{} `json:"data"`
}

type getJobRequest struct {
	S string
}

type updateJobRequest struct {
	S map[string]interface{} `json:"data"`
}

type saveJobResponse struct {
	V   string `json:"result"`
	Err string `json:"err,omitempty"` // errors don't JSON-marshal, so we use a string
}

type getJobResponse struct {
	V   []Job  `json:"result"`
	Err string `json:"err,omitempty"` // errors don't JSON-marshal, so we use a string
}

type updateJobResponse struct {
	V   string `json:"result"`
	Err string `json:"err,omitempty"` // errors don't JSON-marshal, so we use a string
}

package main

import (
	"encoding/json"
	"time"

	"github.com/go-kit/kit/log"
)

type loggingMiddleware struct {
	logger log.Logger
	next   JobQueueService
}

func (mw loggingMiddleware) SaveJob(s map[string]interface{}) (output string, err error) {
	data, err := json.Marshal(s)
	if err != nil {
		panic(err)
	}

	defer func(begin time.Time) {
		_ = mw.logger.Log(
			"method", "savejob",
			"input", string(data),
			"output", output,
			"err", err,
			"took", time.Since(begin),
		)
	}(time.Now())

	output, err = mw.next.SaveJob(s)
	return
}

func (mw loggingMiddleware) GetJob(s string) (output []Job, err error) {
	defer func(begin time.Time) {
		data, err := json.Marshal(output)
		if err != nil {
			panic(err)
		}

		_ = mw.logger.Log(
			"method", "getjob",
			"input", s,
			"output", data,
			"err", err,
			"took", time.Since(begin),
		)
	}(time.Now())

	output, err = mw.next.GetJob(s)
	return
}

func (mw loggingMiddleware) UpdateJob(s map[string]interface{}) (output string, err error) {
	data, err := json.Marshal(s)
	if err != nil {
		panic(err)
	}

	defer func(begin time.Time) {
		_ = mw.logger.Log(
			"method", "updatejob",
			"input", string(data),
			"output", output,
			"err", err,
			"took", time.Since(begin),
		)
	}(time.Now())

	output, err = mw.next.UpdateJob(s)
	return
}

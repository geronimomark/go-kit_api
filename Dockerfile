FROM docker.io/golang

#RUN git clone https://svnsync:5vnm1rr0r@gitlab.usautoparts.com/omp/job-queue-service.git
ADD . /go/src/gitlab.usautoparts.com/omp/job-queue-service

RUN cd /go/src/gitlab.usautoparts.com/omp/job-queue-service
#RUN curl https://glide.sh/get | sh
#RUN glide update
#RUN go build
RUN go install gitlab.usautoparts.com/omp/job-queue-service

ENTRYPOINT /go/bin/job-queue-service

EXPOSE 8081
